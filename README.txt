
=============
== Summary ==
=============
This module provides links to post pages to Juick. Clicking the links will
open a new window or tab with Juick in it. The juick it will be in focus and will
contain a customizable string which can programmatically include the relevant
URL, title, and (if the juick it link appears on a node) taxonomy terms. The
Shorten URLs module is used to shorten the URLs if it is installed.

URLs and titles will be for either the node which is being displayed as a
teaser or for the current page. Multiple links can appear on the same page, as
on a View of teasers. By default, links appear in the Links section when viewing
full nodes or teasers.

==================
== Installation ==
==================
   1. Install this module as usual (FTP the files to sites/all/modules, enable 
        at admin/build/modules).  See http://drupal.org/node/176044 for help.
   2. If you want, go to admin/settings/juick to change the module's settings.

===========
== Links ==
===========
Visit the module page for more information.

Module Page: http://juick.com/unreturned/?tag=juick_it
Enable Module: http://example.com/?q=admin/build/modules
Settings Page: http://example.com/?q=admin/settings/juick
Shorten URLs: http://drupal.org/project/shorten