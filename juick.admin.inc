<?php

/**
 * @file
 * Administration interface for juick module.
 */

/**
 * Settings page.
 */
function juick_admin() {
  $form['juick_node'] = array(
    '#type' => 'select',
    '#title' => t('Type of link to show on nodes'),
    '#default_value' => variable_get('juick_node', 'icon'),
    '#options' => array('icon' => 'icon', 'icon_text' => 'icon_text', 'text' => 'text', 'none' => 'none'),
  );
  $form['juick_teaser'] = array(
    '#type' => 'select',
    '#title' => t('Type of link to show on teasers'),
    '#default_value' => variable_get('juick_teaser', 'none'),
    '#options' => array('icon' => 'icon', 'icon_text' => 'icon_text', 'text' => 'text', 'none' => 'none'),
  );
  $form['juick_new_window'] = array(
    '#type' => 'radios',
    '#title' => t('Open Juick'),
    '#default_value' => variable_get('juick_new_window', 'target'),
    '#options' => array(0 => t('In same window'), 'target' => t('In new window with target="_blank" (not XHTML 1.0 Strict compliant)'), 'js' => t('In new window with JavaScript')),
  );
  $node_types = variable_get('juick_types', array());
  //If all types are selected, un-select them, because the system will still save the result as all selected and it looks better.
  if ($node_types == _juick_node_types()) {
    $node_types = array();
  }
  $form['juick_types'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Node types on which to display link'),
    '#description' => t('If no types are selected, the link will appear on all types.  To stop links from appearing on all nodes, choose "none" in the teaser and node display options above.'),
    '#default_value' => $node_types,
    '#options' => _juick_node_types(),
  );
  $options = array();
  $vocabularies = taxonomy_get_vocabularies();
  foreach ($vocabularies as $key => $vocabulary) {
    $options[$key] = check_plain($vocabulary->name);
  }
  $form['juick_node_vocabs'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Include taxonomy terms from these vocabularies as *tags in juick when used in the current node'),
    '#default_value' => variable_get('juick_node_vocabs', array()),
    '#options' => $options,
  );
  $image_location = drupal_get_path('module', 'juick') .'/icon.png';
  $form['juick_image'] = array(
    '#type' => 'textfield',
    '#title' => t('Image'),
    '#description' => t('Enter the URL for the image you want to show up if you allow images to appear in your links, relative to your Drupal installation.  Ex.: sites/all/modules/juick/icon.png'),
    '#default_value' => variable_get('juick_image', $image_location),
  );
  $form['juick_exclude'] = array(
    '#type' => 'textfield', 
    '#title' => t('Exclude nodes'),
    '#description' => t('Enter the NIDs of nodes which should not have juick links, separated by commas.'),
    '#default_value' => variable_get('juick_exclude', ''),
  );
  $form['juick_format'] = array(
    '#type' => 'textfield',
    '#title' => t('Format'),
    '#description' => t('Manipulate the elements of the juick by changing their order, removing them, or adding them (like *tags).  You can use the case-insensitive tokens [url], [title], and [node-tags].'),
    '#maxlength' => 140,
    '#default_value' => variable_get('juick_format', '[node-tags][title] [url]'),
  );
  $form['juick_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Text of link'),
    '#description' => t('This is only relevant if you picked a display format that uses text.'),
    '#default_value' => variable_get('juick_text', t('Post to juick')),
  );
  return system_settings_form($form);
}

/**
 * Submit handler for juick_admin().
 */
function juick_admin_submit($form, &$form_state) {
  //If no types are selected, assign all types.
  if ($form_state['values']['juick_types'] == array()) {
    $form_state['values']['juick_types'] = _juick_node_types();
  }
  variable_set('juick_types', $form_state['values']['juick_types']);
  //Clear the general cache because changed settings may mean that different URLs should be used.
  cache_clear_all('*', 'cache', TRUE);
}